from django import forms
from ams.models import ItemGroup

class ItemGroupForm(forms.ModelForm):
	class Meta:
		model = ItemGroup
		fields = "__all__"