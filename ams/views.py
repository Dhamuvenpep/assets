from django.shortcuts import render, redirect
from ams.forms import ItemGroupForm
from ams.models import ItemGroup
# Create your views here.
def dashboard(request):
	return render(request, 'desktop/index.html',{})

def accounts(request):
	return render(request, 'desktop/accounts.html',{})

def buy_sell(request):
	return render(request, 'desktop/buy-sell.html',{})

def kyc_app(request):
	return render(request, 'desktop/kyc-application.html',{})

def kyc_form(request):
	return render(request, 'desktop/kyc-form.html',{})

def order_history(request):
	return render(request, 'desktop/order-history.html',{})

def profile_activity(request):
	return render(request, 'desktop/profile-activity.html',{})

def profile_connect(request):
	return render(request, 'desktop/profile-connected.html',{})

def profile_notify(request):
	return render(request, 'desktop/profile-notification.html',{})

def profile_security(request):
	return render(request, 'desktop/profile-security.html',{})

def profile(request):
	return render(request, 'desktop/profile.html',{})

def wallet_bitcoin(request):
	return render(request, 'desktop/wallet-bitcoin.html',{})

def wallet(request):
	return render(request, 'desktop/wallets.html',{})

def welcome(request):
	return render(request, 'desktop/welcome.html',{})

def item(request):
	return render(request, 'desktop/item.html',{})

def create_group(request):
	if request.method=="POST":
		form = ItemGroupForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('/item-group')
	else:
		form = ItemGroupForm()
	return render(request, 'desktop/create_group.html',{})

def item_group(request):
	group_list = ItemGroup.objects.all()
	return render(request, 'desktop/item_group.html', {'group_list':group_list})

def edit_group(request, id):
	group_edit = ItemGroup.objects.get(id=id)
	return render(request, 'desktop/edit_group.html', {'group_edit':group_edit})

def update_group(request, id):
	group_edits = ItemGroup.objects.get(id=id)
	form = ItemGroupForm(request.POST, instance = group_edits)
	if form.is_valid():
		form.save()
		return redirect('/item-group')

def delete_group(request, id):
	group_delete = ItemGroup.objects.get(id=id)
	group_delete.delete()
	group_list = ItemGroup.objects.all()
	return render(request, 'desktop/item_group.html', {'group_list':group_list})