from django.contrib import admin
from ams.models import ItemGroup
from import_export.admin import ImportExportModelAdmin
# Register your models here.
admin.site.register(ItemGroup)