from django.shortcuts import render, redirect
from ams.forms import ItemGroupForm
from ams.models import ItemGroup
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth import login, authenticate
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.hashers import make_password, check_password
# Create your views here.
def admin_login(request):
	if request.session.has_key('username'):
		session_user = request.session['username']
		session_user = session_user.capitalize()
		return redirect('/asset-admin')
	elif request.method == 'POST':
		username = request.POST.get("username", None)
		password = request.POST.get("password", None)
		user = authenticate(username = username, password = password)
		if user is not None:
			request.session['username'] = username
			return HttpResponseRedirect('/asset-admin')
		else:
			messages.error(request, "Invalid username or password.")
	else:
		form = UserCreationForm()
	return render(request, 'admin/admin_login.html', {})

def admin_register(request):
	if request.method == 'POST':
		username = request.POST.get("username", None)
		email = request.POST.get("email", None)
		form = UserCreationForm(request.POST)
		if form.is_valid():
			if form.save():
				return update_user_detail(request, username, email)
			return redirect('/register-success')
	return render(request, 'admin/register.html', {})

def update_user_detail(request, username, email):
	users = User.objects.get(username=username)
	users.is_superuser = 1
	users.is_staff = 1
	users.email = email
	users.save()
	return HttpResponseRedirect('/register-success')

def register_success(request):
	return render(request, 'admin/register_success.html',{})

def asset_admin(request):
	if request.session.has_key('username'):
		return render(request, 'admin/dashboard.html',{})
	else:
		return redirect('/admin-login')

def create_customer(request):
	return render(request, 'admin/create_customer.html',{})

def view_customer(request):
	return render(request, 'admin/view_customer.html',{})