from django.db import models

# Create your models here.
class ItemGroup(models.Model):
	item_group = models.CharField(max_length=100, blank=True, null=True,)
	class Meta:
		db_table = 'item_group'
	def __str__(self):
		return self.item_group