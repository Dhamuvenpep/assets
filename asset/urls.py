"""asset URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from ams import views, admin_views
from django.conf import settings 
from django.conf.urls.static import static
urlpatterns = [
    # Desktop URL
    path('admin/', admin.site.urls),
    path('dashboard', views.dashboard, name='dashboard'),
    path('accounts', views.accounts, name='accounts'),
    path('buy-sell', views.buy_sell, name='buy_sell'),
    path('kyc-app', views.kyc_app, name='kyc_app'),
    path('kyc-form', views.kyc_form, name='kyc_form'),
    path('order-history', views.order_history, name='order_history'),
    path('profile-activity', views.profile_activity, name='profile_activity'),
    path('profile-connect', views.profile_connect, name='profile_connect'),
    path('profile-notify', views.profile_notify, name='profile_notify'),
    path('profile-security', views.profile_security, name='profile_security'),
    path('profile', views.profile, name='profile'),
    path('wallet-bitcoin', views.wallet_bitcoin, name='wallet_bitcoin'),
    path('wallet', views.wallet, name='wallet'),
    path('welcome', views.welcome, name='welcome'),
    path('item', views.item, name='item'),
    path('item-group', views.item_group, name='item_group'),
    path('create-group', views.create_group, name='create_group'),
    path('edit-group/<int:id>', views.edit_group, name='edit_group'),
    path('delete-group/<int:id>', views.delete_group, name='delete_group'),
    path('update-group/<int:id>', views.update_group, name='update_group'),

    # Admin URL
    path('admin-login', admin_views.admin_login, name='admin_login'),
    path('admin-register', admin_views.admin_register, name='admin_register'),
    path('register-success', admin_views.register_success, name='register_success'),
    path('asset-admin', admin_views.asset_admin, name='asset_admin'),
    path('create-customer', admin_views.create_customer, name='create_customer'),
    path('view-customer', admin_views.view_customer, name='view_customer'),
]
if settings.DEBUG:
        urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)